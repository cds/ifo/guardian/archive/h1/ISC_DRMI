# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_DRMI.py 30842 2025-02-28 16:07:54Z david.barker@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_DRMI.py $

import sys
import time
import numpy as np

import cdsutils as cdu

from subprocess import check_output

from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from guardian import NodeManager
import ISC_library
import ISC_GEN_STATES
import lscparams

nominal = 'IDLE'

##################################################
# STATES: INIT / DOWN
##################################################

class INIT(GuardState):
     def run(self):
         return True

class IDLE(GuardState):
    def run(self):
        return True

class LOCKLOSS(GuardState):
    """Record lockloss events"""
    request = False

    def main(self):
        return True

class DOWN(GuardState):
    index = 10
    goto = True

    def main(self):
        # Turn off length loops
        for lsc_loop in ['MICH', 'PRCL', 'SRCL']:
            ezca.get_LIGOFilter('LSC-%s1'%lsc_loop).ramp_gain(0, ramp_time=0, wait=False)
            ezca.get_LIGOFilter('LSC-%s1'%lsc_loop).switch_off('INPUT')

        # Turn off ASC loops
        for py in ['P','Y']:
            for asc_loop in ['MICH', 'PRC1', 'PRC2', 'SRC1', 'SRC2','INP1']:
                ezca.get_LIGOFilter('ASC-%s_%s'%(asc_loop, py)).ramp_gain(0, ramp_time=0, wait=False)
                ezca.get_LIGOFilter('ASC-%s_%s'%(asc_loop, py)).switch_off('INPUT')
            for dof in [1,2,3,4]:
                ezca.get_LIGOFilter('ASC-DC%s_%s'%(dof, py)).switch_off('INPUT')
                ezca['ASC-DC%s_%s_RSET'%(dof, py)] = 2
            time.sleep(0.1)
            for tip_tilt in ['OM1','OM2', 'RM1', 'RM2']:
                ezca['SUS-%s_M1_LOCK_%s_RSET'%(tip_tilt,py)] = 2
            for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2']:
                ezca.get_LIGOFilter('SUS-%s_M1_LOCK_%s'%(sus, py)).ramp_gain(0, ramp_time=1, wait=False)
                ezca.get_LIGOFilter('SUS-%s_M1_LOCK_%s'%(sus, py)).switch_off('INPUT')
            for sus in ['BS', 'PR3']:
                # Treat P and Y the same, JCD 24Jan2020, alog 54709
                ezca.get_LIGOFilter('SUS-%s_M1_LOCK_%s'%(sus, py)).switch_off('INPUT')
                ezca.get_LIGOFilter('SUS-%s_M1_LOCK_%s'%(sus, py)).ramp_gain(0, ramp_time=1, wait=False)
                #if py == 'P':
                #    ezca.get_LIGOFilter('SUS-%s_M1_LOCK_P'%sus).switch_off('FM1')
                #elif py == 'Y':
                #    ezca.get_LIGOFilter('SUS-%s_M1_LOCK_Y'%sus).ramp_gain(0, ramp_time=1, wait=False)

        # Clear Length integrators, reset oplevs
        for sus in ['PRM', 'BS', 'SRM']:
            ezca.get_LIGOFilter('SUS-%s_M1_LOCK_L'%sus).ramp_gain(0, ramp_time=1, wait=False)
            ezca.get_LIGOFilter('SUS-%s_M1_LOCK_L'%sus).switch_off('INPUT')
            if sus == 'BS':
                ezca.get_LIGOFilter('SUS-%s_M1_LOCK_L'%sus).switch_off('FM1')
                ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_P').switch_on('INPUT','OUTPUT', wait=False)
                ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_Y').switch_on('INPUT','OUTPUT', wait=False)
                ezca['SUS-BS_M2_OLDAMP_P_GAIN'] = 300  # BS oplev back on
                ezca['SUS-BS_M2_OLDAMP_Y_GAIN'] = 650
            else: # PRM and SRM

                ezca.get_LIGOFilter('SUS-%s_M2_LOCK_L'%sus).switch_off('INPUT')
                ezca.get_LIGOFilter('SUS-%s_M2_LOCK_L'%sus).ramp_gain(0, ramp_time=1, wait=False)

        # put back in regular eul2osem matrix
        coilburtpath = '/opt/rtcds/userapps/release/isc/h1/scripts/sus/'
        for sus in ['PRM','PR2','SRM','SR2','BS']:
            if sus == 'BS':
                stage = 'M2'
            else:
                stage = 'M3'
            ezca.burtwb(coilburtpath+ sus.lower() + '_' + stage.lower() +'_out_normal.snap')

        self.timer['ramp_wait'] = 10  # ensure ramps are complete before resetting filter banks in run state

        # turn off OMC DC centering
        ezca['OMC-ASC_MASTERGAIN'] = 0
        ezca['OMC-ASC_QDSLIDER'] = 1
        for dof in ['X','Y']:
            for basis in ['ANG','POS']:
                ezca['OMC-ASC_%s_%s_RSET'%(basis,dof)] = 2

        # turn off SRM dither, ADS
        for py in ['PIT','YAW']:
            ezca['ASC-ADS_%s1_OSC_CLKGAIN'%py] = 0
            for dof in [1,2]:
                ezca.get_LIGOFilter('ASC-ADS_%s%s_DOF'%(py,dof)).ramp_gain(0, ramp_time=0, wait=False)
                ezca['ASC-ADS_%s%s_DOF_RSET'%(py,dof)] = 2

        # set trigger thresholds back
        trigger_threshs = [('LSC-MICH_TRIG_THRESH_ON', lscparams.thresh['DRMI_MICH_noarms']['ON']),
                           ('LSC-MICH_TRIG_THRESH_OFF', lscparams.thresh['DRMI_MICH_noarms']['OFF']),
                           ('LSC-MICH_FM_TRIG_THRESH_ON', lscparams.thresh['DRMI_MICH_FMs']['ON']),
                           ('LSC-MICH_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_MICH_FMs']['OFF']),
                           ('LSC-SRCL_TRIG_THRESH_ON', lscparams.thresh['DRMI_SRCL_noarms']['ON']),
                           ('LSC-SRCL_TRIG_THRESH_OFF', lscparams.thresh['DRMI_SRCL_noarms']['OFF']),
                           ('LSC-SRCL_FM_TRIG_THRESH_ON', lscparams.thresh['DRMI_SRCL_noarms_FMs']['ON']),
                           ('LSC-SRCL_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_SRCL_noarms_FMs']['OFF']),
                           ('LSC-PRCL_FM_TRIG_THRESH_ON', lscparams.thresh['DRMI_PRCL_FMs']['ON']),
                           ('LSC-PRCL_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_PRCL_FMs']['OFF'])
        ]
        for chan in trigger_threshs:
             ezca[chan[0]] = chan[1]

        ezca['LSC-CONTROL_ENABLE'] = 1

        time.sleep(10)
        for lsc_loop in ['MICH', 'PRCL', 'SRCL']:
            ezca['LSC-%s1_RSET'%lsc_loop] = 2

        for py in ['P','Y']:
            for asc_loop in ['MICH', 'PRC1', 'PRC2', 'SRC1', 'SRC2','INP1']:
                ezca['ASC-%s_%s_RSET'%(asc_loop, py)] = 2
            for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2']:
                ezca['SUS-%s_M1_LOCK_%s_RSET'%(sus, py)] = 2
            for sus in ['BS', 'PR3']:
                # Treat P and Y the same, JCD 24Jan2020, alog 54709
                ezca['SUS-%s_M1_LOCK_%s_RSET'%(sus, py)] = 2


        # PRM, SRM length M2 feedback
        for sus in ['PRM','SRM']:
            ezca['SUS-%s_M2_LOCK_L_RSET'%sus] = 2

        time.sleep(0.1)

        # reset the gains (BS and PR3 only reset yaw b/c wire heating)
        for py in ['P','Y']:
            for sus in ['IM4', 'SRM', 'SR2', 'PRM', 'PR2']:
                ezca['SUS-%s_M1_LOCK_%s_GAIN'%(sus,py)] = 1

        #Adding pit gain setting for BS/PR3, CG 27Jan2020, alog54743
        ezca['SUS-BS_M1_LOCK_P_GAIN'] = -1
        ezca['SUS-PR3_M1_LOCK_P_GAIN'] = 1

        ezca['SUS-BS_M1_LOCK_Y_GAIN'] = -1
        ezca['SUS-PR3_M1_LOCK_Y_GAIN'] = 1

        # undo gain increase for SRM, PRM offloading
        for sus in ['PRM','SRM']:
            ezca['SUS-%s_M2_LOCK_L_GAIN'%sus] = 1

        for sus in ['PRM','PR2','SRM','SR2','BS']:
            if sus == 'BS':
                stage = 'M2'
            else:
                stage = 'M3'
            ezca['SUS-%s_%s_EUL2OSEM_LOAD_MATRIX'%(sus,stage)] = 1

    def run(self):
        # Final check, then done
        if ISC_library.is_locked('IMC'):
            return True
        else:
            notify('MC is not locked')

##################################################
# STATES: DRMI length
##################################################

class PREP_DRMI(GuardState):
    index = 20
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):

        # Moved to prep from DOWN, so that it definitely happens after an initial alignment. JCD SED 16Aug2019
        # set lower sus stage coil drivers to high range
        # Moved BS switch from here to ISC_LOCK DOWN state to prevent oplev damping loop saturations after lock loss 
        # 05Dec2019 JSK, TJS, JCD
        for sus in ['PRM', 'SRM']:
            if sus == 'PRM':
                ezca['SUS-PRM_BIO_M2_STATEREQ'] = 3
            for coil in ['UR','UL','LR','LL']:
                ezca['SUS-%s_BIO_M3_%s_STATEREQ'%(sus,coil)] = 2

        # BS length M1 feed-back setup
        ezca['SUS-BS_M1_LOCK_L_GAIN'] = 0.1

        # prep LSC control settings
        ezca.get_LIGOFilter('LSC-MICH2').only_on('INPUT','FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        #ezca.get_LIGOFilter('LSC-MICH2').only_on('INPUT','FM1', 'OUTPUT', 'DECIMATION') # HY removed the FM2 LP50 in MICH2 and replaced it with LP8 in MICH1 FM5
        # MICH1 FMs are defined in lscparams, and used in the next set of code.  All lowpasses were turned off for acquisition, which wasn't good.

        for dof in ['MICH', 'PRCL', 'SRCL']:
            ISC_library.intrix.zero(row=dof)
            ezca.get_LIGOFilter('LSC-%s1'%dof).only_on('DECIMATION', 'OUTPUT')
            ezca['LSC-%s2_GAIN'%dof]=1
            for jj in lscparams.filtmods[dof]:
                ezca.get_LIGOFilter('LSC-%s1'%dof).switch_on(jj)

        # turn off sensing matrix notch in case it was left on
        ezca.get_LIGOFilter('LSC-PRCL2').switch_off('FM6')
        ezca.get_LIGOFilter('LSC-SRCL2').switch_off('FM6')

        # set LSC input matrix
        ISC_library.intrix['MICH', 'REFLAIR_A45Q'] = lscparams.lsc_mtrx_elems['MICH']
        ISC_library.intrix['PRCL', 'REFLAIR_A9I']  = lscparams.lsc_mtrx_elems['PRCL']
        ISC_library.intrix['SRCL', 'REFLAIR_A45I'] = lscparams.lsc_mtrx_elems['SRCL']
        ISC_library.intrix['SRCL', 'REFLAIR_A9I']  = -1.8 
        time.sleep(0.1)
        ISC_library.intrix.load()

        # set LSC output matrix
        ISC_library.outrix['PRM', 'PRCL'] = 1
        ISC_library.outrix['SRM', 'SRCL'] = 1
        ISC_library.outrix['BS',  'MICH'] = 1

        # set trigger matrix
        for dof in ('MICH', 'PRCL', 'SRCL'):
            ISC_library.trigrix.zero(row=dof)
            ISC_library.trigrix[dof, 'POPAIR_B_RF18_I'] = 1

        # set delayed trigger for SRM turn-on
        ezca['LSC-PRCL_FM_TRIG_WAIT'] = lscparams.trigDelay['PRCL_FM']
        ezca['LSC-MICH_FM_TRIG_WAIT'] = lscparams.trigDelay['MICH_FM']
        ezca['LSC-SRCL_FM_TRIG_WAIT'] = lscparams.trigDelay['SRCL_FM']

        ezca['LSC-REFLBIAS_TRIG_THRESH_ON'] = -100
        ezca['LSC-REFLBIAS_TRIG_THRESH_OFF'] = -100

        # set FM trigger mask
        for dof in ['MICH','PRCL','SRCL']:
            for jj in lscparams.trig_mask[dof]:
                if jj in lscparams.trig_mask[dof]:
                    ezca['LSC-%s_MASK_FM%d'%(dof,jj)] = 1
                else:
                    ezca['LSC-%s_MASK_FM%d'%(dof,jj)] = 0

        # set trigger thresholds
        trigger_threshs1 = [('LSC-PRCL_TRIG_THRESH_ON',     lscparams.thresh['DRMI_PRCL']['ON']),
                           ('LSC-PRCL_TRIG_THRESH_OFF',    lscparams.thresh['DRMI_PRCL']['OFF']),
                           ('LSC-PRCL_FM_TRIG_THRESH_ON',  lscparams.thresh['DRMI_PRCL_FMs']['ON']),
                           ('LSC-PRCL_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_PRCL_FMs']['OFF'])
        ]
        for chan in trigger_threshs1:
             ezca[chan[0]] = chan[1]

        if ezca.read('GRD-SUS_ETMX_STATE', as_string=True) == 'MISALIGNED' or lscparams.gate_valve_flag:
            log('setting mich triggers for DRMI without arms')
            trigger_threshs2 = [('LSC-MICH_TRIG_THRESH_ON',     lscparams.thresh['DRMI_MICH_noarms']['ON']),
                                ('LSC-MICH_TRIG_THRESH_OFF',    lscparams.thresh['DRMI_MICH_noarms']['OFF']),
                                ('LSC-MICH_FM_TRIG_THRESH_ON',  lscparams.thresh['DRMI_MICH_FMs_noarms']['ON']),
                                ('LSC-MICH_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_MICH_FMs_noarms']['OFF']),
                                ('LSC-SRCL_TRIG_THRESH_ON',     lscparams.thresh['DRMI_SRCL_noarms']['ON']),
                                ('LSC-SRCL_TRIG_THRESH_OFF',    lscparams.thresh['DRMI_SRCL_noarms']['OFF']),
                                ('LSC-SRCL_FM_TRIG_THRESH_ON',  lscparams.thresh['DRMI_SRCL_noarms_FMs']['ON']),
                                ('LSC-SRCL_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_SRCL_noarms_FMs']['OFF'])
            ]
            for chan in trigger_threshs2:
                 ezca[chan[0]] = chan[1]

        else:
            log('setting MICH trigger thresholds for DRMI with arms')
            trigger_thresh2 = [('LSC-MICH_TRIG_THRESH_ON',     lscparams.thresh['DRMI_MICH_als']['ON']),
                               ('LSC-MICH_TRIG_THRESH_OFF',    lscparams.thresh['DRMI_MICH_als']['OFF']),
                               ('LSC-MICH_FM_TRIG_THRESH_ON',  lscparams.thresh['DRMI_MICH_FMs']['ON']),
                               ('LSC-MICH_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_MICH_FMs']['OFF']),
                               ('LSC-SRCL_TRIG_THRESH_ON',     lscparams.thresh['DRMI_SRCL_als']['ON']),
                               ('LSC-SRCL_TRIG_THRESH_OFF',    lscparams.thresh['DRMI_SRCL_als']['OFF']),
                               ('LSC-SRCL_FM_TRIG_THRESH_ON',  lscparams.thresh['DRMI_SRCL_als_FMs']['ON']),
                               ('LSC-SRCL_FM_TRIG_THRESH_OFF', lscparams.thresh['DRMI_SRCL_als_FMs']['OFF'])
            ]
            for chan in trigger_thresh2:
                 ezca[chan[0]] = chan[1]


        # prep suspensions
        # BS
        ezca['SUS-BS_M3_ISCINF_L_LIMIT'] = 500000  # BS limiter ON
        ezca.get_LIGOFilter('SUS-BS_M3_ISCINF_L').switch('LIMIT', 'ON', wait=True)
        ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'ON')

        ezca.get_LIGOFilter('SUS-BS_M3_ISCINF_L').switch('FM4','FM5','FM6', 'OFF', wait=False)
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_off('INPUT','FM1') # Turn off feedback to M1 stage of BS
        ezca.get_LIGOFilter('SUS-BS_M2_LOCK_L').switch('FM4', 'FM5', 'FM9' , 'FM10', 'ON')

        # ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'ON') # removed 20180927. The 35Hz ELP seemed to make DRMI locking harder

        # PRM: Turn off feedback to M2 stage then prepare the filters for offloading
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca['SUS-PRM_M2_LOCK_OUTSW_L'] = 0
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').only_on('FM2', 'FM5', 'FM6', 'FM7', 'FM8',    # new offload configuration (Gabriele 2023-04-21 https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=68899)
                             'FM10', 'INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').only_on('INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').ramp_gain(1, 2, wait=False)
        ezca.get_LIGOFilter('SUS-PRM_M3_ISCINF_L').switch_off('FM3') # Remove stopband until after locked. JCD, SB, 27Sept2018.

        # SRM: Turn off feedback to M2 stage then prepare the filters for offloading
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca['SUS-SRM_M2_LOCK_OUTSW_L'] = 0
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').only_on('FM2', 'FM5', 'FM6', 'FM7', 'FM8',  # new offload configuration (Gabriele 2023-04-21 https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=68899)
                             'FM10', 'INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').only_on('INPUT', 'OUTPUT', 'DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').ramp_gain(0, 2, wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').ramp_gain(1, 2, wait=False)

        #new stuff for fast DRMI AUgust 2018
        ezca.get_LIGOFilter('LSC-SRCL1').switch_on('FM2')
        ezca.get_LIGOFilter('LSC-SRCL1').switch_on('FM1')

        ezca['LSC-PR_GAIN_GAIN'] = 0  #get rid of annoying flashes on strip tool

        log('ready for locking')

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        return True



class ACQUIRE_DRMI_1F(GuardState):
    # combination of old LOCK_DRMI_1F and DRMI_LOCK_WAIT
    index = 30
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        if not ISC_library.REFL_PD_OK():
            notify('No light on REFL PD')

        for dof in ['MICH', 'PRCL', 'SRCL']:
            ezca.get_LIGOFilter('LSC-%s1'%dof).switch('INPUT','OUTPUT', 'ON', wait=False)
            ezca.get_LIGOFilter('LSC-%s2'%dof).switch('INPUT','OUTPUT', 'ON', wait=False)
            if ezca.read('GRD-SUS_ETMX_STATE', as_string=True) == 'MISALIGNED' or lscparams.gate_valve_flag == True:
                log('setting gains for DRMI without arms')
                ezca.get_LIGOFilter('LSC-%s1'%dof).ramp_gain(lscparams.gain['DRMI_%s'%dof]['NO_ARMS'], ramp_time=1, wait=False)
            else:
                log('setting gains for DRMI with arms')
                ezca.get_LIGOFilter('LSC-%s1'%dof).ramp_gain(lscparams.gain['DRMI_%s'%dof]['ACQUIRE'], ramp_time=1, wait=False)
        ezca.get_LIGOFilter('LSC-XARM').switch('INPUT','OUTPUT', 'ON', wait=False)
        ezca.get_LIGOFilter('LSC-XARM').switch('OFFSET','OFF')
        #ezca.get_LIGOFilter('LSC-SRCL1').switch('OFFSET','OFF') # ensure modehop offset is off before trying to acquire
        self.lock_timer_on = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        #if the target is LOCK_PRMI, go there
        if ezca.read('GRD-ISC_DRMI_TARGET', as_string=True)=='PREP_PRMI':
            log('request has changed, returning true')
            return True

        if ISC_library.is_locked('DRMI'):
            # look for a lock longer than some minimum time before proceeding
            if self.lock_timer_on:
                if self.timer['lockpause']:
                    log('done waiting, going to DRMI_LOCKED')
                    return True
                else:
                    return
            else:
                log('flashed, waiting to proceed')
                self.timer['lockpause'] = 0.5
                self.lock_timer_on = True
        else:
            if self.lock_timer_on:
                self.lock_timer_on=False


class DRMI_LOCKED(GuardState):
    # Formerly known as OFFLOAD_DRMI
    index = 50
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        log('In DRMI_LOCKED')
        if ezca.read('GRD-ISC_DRMI_MODE', as_string=True) != 'MANAGED' and not ISC_library.is_locked('DRMI'):
            log('is_locked("DRMI") returned False, returning to PREP_DRMI')
            return 'PREP_DRMI'

        else:
            # Set final DRMI gains
            for dof in ['MICH', 'PRCL', 'SRCL']:
                if ezca.read('GRD-SUS_ETMX_STATE', as_string=True) == 'MISALIGNED' or lscparams.gate_valve_flag == True:
                    log('setting gains for DRMI without arms')
                    ezca.get_LIGOFilter('LSC-%s1'%dof).ramp_gain(lscparams.gain['DRMI_%s'%dof]['NO_ARMS'], ramp_time=2, wait=False)
                else:
                    log('setting gains for DRMI with arms')
                    ezca.get_LIGOFilter('LSC-%s1'%dof).ramp_gain(lscparams.gain['DRMI_%s'%dof]['W_ALS'], ramp_time=2, wait=False)

            ezca['LSC-SRCL1_OFFSET'] = lscparams.offset['SRCL_MODEHOP']
            ezca.get_LIGOFilter('LSC-SRCL1').switch_on('OFFSET') # offset with 50W TCS preloading is bad. JCD 29Nov2018
            ezca.get_LIGOFilter('LSC-SRCL1').switch_on('FM4')
            ezca.get_LIGOFilter('LSC-PRCL1').switch_on('FM3')
            ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'OFF') # BS limiter OFF
            ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'OFF')

        # Turn on feedback to M1 stages of PRM and SRM
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(lscparams.sus_crossover['PRM_M1'], ramp_time=2, wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').ramp_gain(lscparams.sus_crossover['SRM_M1'], ramp_time=2, wait=False)
        log('Offloaded PRM and SRM')

        ezca.get_LIGOFilter('SUS-PRM_M3_ISCINF_L').switch_on('FM3') # put stopband back in once locked.

        # Turn off CPS FF to BS (so called MICH freeze)
        ezca['LSC-CPSFF_GAIN'] = 0
        ezca['SUS-BS_M3_ISCINF_L_OFFSET'] = 0

        ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch_off('FM2') #is already off

        log('Made it through DRMI_LOCKED main state, waiting 1 second')
        self.timer['wait'] = 1
        self.counter = 1

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        if not ISC_library.is_locked('DRMI'):
            return 'PREP_DRMI'

        if self.timer['wait'] and self.counter == 1:
            log('DRMI_LOCKED run counter = %d'%self.counter)
            #ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2')
            ezca.get_LIGOFilter('LSC-MICH2').switch_off('FM2')
            ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM1', 'FM5')
            ezca.get_LIGOFilter('LSC-MICH2').switch_on('FM3')
            ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').only_on('INPUT', 'FM2', 'OUTPUT', 'DECIMATION') # turn on BS top stage feedback

            self.timer['wait'] = 2
            self.counter += 1

        elif self.timer['wait'] and self.counter == 2:
            log('DRMI_LOCKED run counter = %d'%self.counter)
            ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_on('FM1')
            ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').switch_on('FM1')
            ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').switch_on('FM1')

            ezca.get_LIGOFilter('SUS-BS_M2_LOCK_L').switch_off('FM10')  # Turn BounceRoll off in BS SUS
            #ezca.get_LIGOFilter('LSC-MICH2').switch_on('FM10') # Turn on the BounceRoll FM10 in LSC FM
            ezca.get_LIGOFilter('LSC-MICH2').switch_on('FM8') # Turn on the BounceRoll FM8 in LSC FM 05/06/2024 J Wright
            log('Done with DRMI_LOCKED run')
            return True


class DRMI_1F_READY(GuardState):
    index = 51
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        log('ISC_DRMI DRMI_1F_READY Reached')

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        return True


##################################################
# STATES: PRMI length
##################################################

class PREP_PRMI(GuardState):
    index = 32
    reqest = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        ezca['LSC-MICH_TRIG_THRESH_ON'] = lscparams.thresh['PRMIsb_MICH']['ON']
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = lscparams.thresh['PRMIsb_MICH']['OFF']

        ezca.get_LIGOFilter('LSC-SRCL1').switch('INPUT','OFFSET','OUTPUT', 'OFF', wait=False)
        time.sleep(0.1)
        ezca['LSC-SRCL1_RSET'] = 2

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        return True


class ACQUIRE_PRMI(GuardState):
    index = 33
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        if not ISC_library.REFL_PD_OK():
            notify('No light on REFL PD')

        self.lock_timer_on = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        if ezca.read('GRD-ISC_DRMI_TARGET', as_string=True)=='PREP_DRMI':
            log('request has changed, returning true')
            return True

        if ISC_library.is_locked('PRMI'):
            log('I think its locked')
            # look for a lock longer than some minimum time before proceeding
            if self.lock_timer_on:
                if self.timer['lockpause']:
                    ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['PRMI_MICH']['W_ALS'], ramp_time=5, wait=False)  # temp changed 2 -> 4 due to prmi issues 05/19/24
                    ezca.get_LIGOFilter('LSC-PRCL1').ramp_gain(lscparams.gain['PRMI_PRCL']['W_ALS'], ramp_time=5, wait=False)  # temp changed 2 -> 4 due to prmi issues 05/19/24
                    ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'OFF') # BS limiter OFF
                    ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'OFF')
                    #this was removed in the guardian changes from March
                    ezca['LSC-SRCL1_RSET'] = 2
                    return True
                else:
                    return
            else:
                log('flashed, waiting to proceed')
                self.timer['lockpause'] = 0.5
                self.lock_timer_on = True
        else:
            if self.lock_timer_on:
                self.lock_timer_on=False


class ACQUIRE_PRMI_CARRIER(GuardState):
    index = 34
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        ISC_library.trigrix['MICH','POP_A_DC'] = 1
        ISC_library.trigrix['PRCL','POP_A_DC'] = 1
        ISC_library.trigrix['MICH','POPAIR_B_RF18_I'] = 0
        ISC_library.trigrix['PRCL','POPAIR_B_RF18_I'] = 0

        ezca['LSC-MICH_TRIG_THRESH_ON']  = lscparams.thresh['PRMIcar_MICH']['ON']
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = lscparams.thresh['PRMIcar_MICH']['OFF']
        ezca['LSC-PRCL_FM_TRIG_THRESH_ON'] = 50
        ezca['LSC-MICH_FM_TRIG_THRESH_ON'] = 40
        ezca['LSC-MICH_FM_TRIG_THRESH_ON'] = 185
        ezca['LSC-PRCL1_GAIN']           = lscparams.gain['PRMI_PRCL']['CARRIER']
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(lscparams.gain['PRMI_MICH']['W_ALS'], ramp_time=2, wait=False)

        ezca.get_LIGOFilter('LSC-SRCL1').switch('INPUT','OFFSET','OUTPUT', 'OFF', wait=False)
        time.sleep(0.1)
        ezca['LSC-SRCL1_RSET'] = 2

        ISC_library.intrix.put('MICH',[],0) # Zero out MICH row of input matrix, 
        ISC_library.intrix.put('PRCL',[],0) # Zero out PRCL row of input matrix,

        ISC_library.intrix['PRCL', 'REFL_A45I']  = 240
        ISC_library.intrix['MICH', 'REFL_A45Q'] = 4000
        ISC_library.intrix.load()
        #zero REFLAIR sensors
        #refl 45 I to PRCL
        #refl 45 Q tp MICH

        self.lock_timer_on = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        if ezca.read('GRD-ISC_DRMI_TARGET', as_string=True)=='PREP_DRMI':
            log('request has changed, returning true')
            return True

        if ISC_library.is_locked('PRMI'):
            if self.lock_timer_on: # look for a lock longer than some minimum time before proceeding
                log('lock timer on')
                if self.timer['lockpause']:
                    log('for more than timer')
                    ezca.get_LIGOFilter('SUS-BS_M3_ISCINF_L').switch_off('LIMIT') # BS limiter OFF
                    ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'OFF')
                    ezca['LSC-SRCL1_RSET'] = 2
                    return True
                else:
                    return
            else:
                log('flashed, waiting to proceed')
                self.timer['lockpause'] = 0.5
                self.lock_timer_on = True
        else:
            if self.lock_timer_on:
                self.lock_timer_on=False


class PRMI_LOCKED(GuardState):
    index = 35
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        #if node is not managed, check for lock.  If it is managed let ISC_LOCK manage it so that we avoid stalling the node.
        if ezca.read('GRD-ISC_DRMI_MODE', as_string=True) != 'MANAGED' and not ISC_library.is_locked('PRMI'):
            # go through prep DRMI, acq DRMI, then to prep PRMI, acq PRMI, then here
            return 'PREP_DRMI'

        # Turn on feedback to M1 stage of PRM
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(lscparams.sus_crossover['PRM_M1'], ramp_time=2, wait=False)
        log('Offloaded PRM')
        ezca.get_LIGOFilter('SUS-PRM_M3_ISCINF_L').switch_on('FM3') # put stopband back in once locked.
        # Set final PRMI gains (actually uses DRMI values)
        for dof in ['MICH', 'PRCL']:
            if ezca.read('GRD-SUS_ETMX_STATE', as_string=True) == 'MISALIGNED' or lscparams.gate_valve_flag:
                log('setting gains for PRMI without arms')
                ezca.get_LIGOFilter('LSC-%s1'%dof).ramp_gain(lscparams.gain['PRMI_%s'%dof]['NO_ARMS'], ramp_time=2, wait=False)
            else:
                log('setting gains for PRMI with arms')
                ezca.get_LIGOFilter('LSC-%s1'%dof).ramp_gain(lscparams.gain['PRMI_%s'%dof]['W_ALS'], ramp_time=2, wait=False)
                time.sleep(5)  # 05/19/24 prmi troubleshooting

        # Turn off CPS FF to BS (so called MICH freeze)
        ezca['LSC-CPSFF_GAIN'] = 0
        ezca['SUS-BS_M3_ISCINF_L_OFFSET'] = 0

        self.timer['wait'] = 1
        self.counter = 1

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        if not ISC_library.is_locked('PRMI'):
            # go through prep DRMI, acq DRMI, then to prep PRMI, acq PRMI, then here
            return 'PREP_DRMI'

        if self.timer['wait'] and self.counter == 1:
            #ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2')
            ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_on('INPUT') # turn on BS top stage feedback

            self.timer['wait'] = 2
            self.counter += 1

        elif self.timer['wait'] and self.counter == 2:
            ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').switch_on('FM1')
            self.counter +=1
        elif self.counter >=3:
            return True


PRMI_WFS_CENTERING = ISC_GEN_STATES.gen_WFS_DC_CENTERING('PRMI','REFL_AS')
PRMI_WFS_CENTERING.index = 36

class PREP_PRMI_ASC(GuardState):
    index = 37
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','PRMI'])
    def main(self):
        ezca['ASC-WFS_SWTCH'] = 1
        ezca['ASC-WFS_GAIN']  = 1

        for dof in ['MICH','INP1','INP2','PRC1','PRC2','SRC1','SRC2']:
            for py in ['PIT','YAW']:
                ISC_library.asc_intrix[py].put(dof,[],0) # Zero out rows of input matrix
                ISC_library.asc_outrix[py].put([],dof,0) # Zero out columns of output matrix

        # INPUT MATRIX
        for py in ['PIT','YAW']:
            # For settings that are the same pit and yaw
            ISC_library.asc_intrix[py]['MICH', 'AS_B_RF45_Q']  = -10
            ISC_library.asc_intrix[py]['PRC1', 'REFL_A_RF9_I']     = 0.001*2  #correcting for HAM1 power splitter June 23 2022

        # OUTPUT MATRIX
        for py in ['PIT','YAW']:
            ISC_library.asc_outrix[py]['IM4', 'INP1'] = 1
            ISC_library.asc_outrix[py]['PRM', 'PRC1'] = 1
            ISC_library.asc_outrix[py]['PR2', 'PRC2'] = 1
            ISC_library.asc_outrix[py]['BS',  'MICH'] = 1
            ISC_library.asc_outrix[py]['SRM', 'SRC1'] = 1
            ISC_library.asc_outrix[py]['SR2', 'SRC2'] = 1
        ISC_library.asc_outrix['PIT']['SRM',  'SRC2'] = -7.6 # to remove SRM coupling into SR2
        ISC_library.asc_outrix['YAW']['SRM',  'SRC2'] = 7.1

        ezca['ASC-PRC1_P_LIMIT'] = 10000
        ezca['ASC-PRC1_Y_LIMIT'] = 10000

        # SET UP CONTROL SETTINGS
        time.sleep(0.5)

        ezca.get_LIGOFilter('ASC-MICH_P').only_on('INPUT','FM3','FM9','FM10','OUTPUT', 'DECIMATION') #SED increased gain by 20db (removing FM1) Oct 28 2019. added in BR for damped mode at 17Hz 05/06/2024 J Wright
        #ezca.get_LIGOFilter('ASC-MICH_P').only_on('INPUT', 'FM1','FM4','FM8','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-MICH_Y').only_on('INPUT','FM3','FM4','FM7','FM9','FM10','OUTPUT', 'DECIMATION')#change LP from LP6 to ELP15 SED DDB March 17 2019, added in BR for damped mode at 17Hz 05/06/2024 J Wright

        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM1', 'FM3', 'FM7', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM1', 'FM3', 'FM7', 'OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = -0.1
        ezca['ASC-PRC1_Y_GAIN'] = -0.1

        ezca.get_LIGOFilter('ASC-PRC2_P').only_on('FM3', 'FM4', 'FM8','OUTPUT', 'DECIMATION') #EMC switched FM7 to FM8 20220816, modified plant inversion to prevent ringing
        ezca.get_LIGOFilter('ASC-PRC2_Y').only_on('FM3', 'FM4', 'FM8','OUTPUT','DECIMATION')#20190122 switched to match full ifo asc, was fm 2 3 4 5 10
        ezca['ASC-PRC2_P_GAIN'] = -3
        ezca['ASC-PRC2_Y_GAIN'] = -1 #20190122, increased for faster convergence, was -0.5


        # set up BS feedback to M2 only, hold M1
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P').only_on('FM1', 'FM2', 'FM5', 'FM9', 'LIMIT','OUTPUT', 'DECIMATION') #m1 input turned off
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y').only_on('FM1', 'FM5', 'FM9', 'LIMIT','OUTPUT', 'DECIMATION')
        # HY 09/04/18 adding extra LP0.5 (FM5) for the top stage
        ezca['SUS-BS_M2_LOCK_OUTSW_P'] = 1
        ezca['SUS-BS_M2_LOCK_OUTSW_Y'] = 1
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P').switch_on('INPUT') # Used to be in ENGAGE - why not here?
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y').switch_on('INPUT')# Used to be in ENGAGE - why not here?

        # set up PRM feedback to M3
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_P').only_on('INPUT','FM1','FM9', 'LIMIT','OUTPUT', 'DECIMATION') #m1 input is turned off
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_Y').only_on('INPUT','FM1','FM9', 'LIMIT','OUTPUT', 'DECIMATION')    #sed,crc turned on because it seems fine to use it see alog 43259 and comments
        ezca['SUS-PRM_M3_LOCK_OUTSW_P'] = 1
        ezca['SUS-PRM_M3_LOCK_OUTSW_Y'] = 1


    @ISC_library.assert_dof_locked_gen(['IMC','PRMI'])
    def run(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL_AS'):
            notify('WFS DC centering railed, consider checking HAM6 HV')
        else:
            return True

class ENGAGE_PRMI_ASC(GuardState):
    index = 38
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','PRMI'])
    def main(self):
        self.counter = 1
        self.timer['wait'] = 0
        self.timer['dt'] = 1./16.
        self.useMICH = True
        self.usePRC1 = True

    @ISC_library.assert_dof_locked_gen(['IMC','PRMI'])
    def run(self):

        if self.counter == 1:
            if self.useMICH:
                #ezca.get_LIGOFilter('ASC-MICH_P').ramp_gain(-0.3, ramp_time=10, wait=False)
                ezca.get_LIGOFilter('ASC-MICH_P').ramp_gain(lscparams.asc_gains['MICH']['P']['ENGAGE_PRMI_ASC'], ramp_time=20, wait=False)
                # was -0.22 # SD, HY changed the MICH gain so that the UGF is 0.7 Hz; to avoid osc. at 1.1 Hz
                ezca.get_LIGOFilter('ASC-MICH_Y').ramp_gain(lscparams.asc_gains['MICH']['Y']['ENGAGE_PRMI_ASC'], ramp_time=20, wait=False)
            if self.usePRC1:
                ezca.get_LIGOFilter('ASC-PRC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-PRC1_Y').switch_on('INPUT')

            self.counter += 1
            self.timer['wait'] = ezca['ASC-MICH_P_TRAMP']

        if self.counter == 2 and self.timer['wait']:
            if self.useMICH:
                ezca.get_LIGOFilter('ASC-MICH_P').switch_on('FM2')
                ezca.get_LIGOFilter('ASC-MICH_Y').switch_on('FM2')

            self.counter += 1
            self.timer['wait'] = 4

        if self.counter == 3 and self.timer['wait']:
            #this is where we used to turn on PRC1, but we are trying engaging PRC1 at the same time as MICH as of early July 20
            self.counter += 1

        if self.counter == 4 and self.timer['wait']:
            return True

class PRMI_ASC(GuardState):
    index = 39
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC','PRMI'])
    def run(self):
        LoopList  = ['MICH', 'PRC1']
        errTolerancePit = [40, 1]
        errToleranceYaw = [40, 1]
        ctrlTolerancePit = [2, 100]
        ctrlToleranceYaw = [2, 100]
        if ISC_library.asc_err_convergence_checker(LoopList, errTolerancePit, errToleranceYaw) and ISC_library.asc_convergence_checker(LoopList, ctrlTolerancePit, ctrlToleranceYaw):
            return True


OFFLOAD_PRMI_ASC = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('PRMI', 5, ['BS','PRM'])
OFFLOAD_PRMI_ASC.index = 40

class PRMI_ASC_OFFLOADED(GuardState):
    index = 41
    request = True
    @ISC_library.assert_dof_locked_gen(['IMC','PRMI'])
    #@nodes.checker()
    def main(self):
        ezca.get_LIGOFilter('ASC-MICH_P').ramp_gain(0,3,wait=False)
        ezca.get_LIGOFilter('ASC-MICH_Y').ramp_gain(0,3,wait=False)
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_P').ramp_gain(0,3,wait=False)
        ezca.get_LIGOFilter('ASC-PRC1_Y').ramp_gain(0,3,wait=False)
        for AS_centering in ['3','4']:
            for dof in ['P', 'Y']:
                ezca.get_LIGOFilter('ASC-DC%s_%s'%(AS_centering, dof)).switch_off('INPUT')
        return True


class PREP_PRMI_TO_DRMI(GuardState):
    index = 48
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        ezca.get_LIGOFilter('SUS-BS_M3_ISCINF_L').switch_on('LIMIT')
        ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'ON')
        # Change SRCL trigger
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = lscparams.thresh['DRMI_SRCL_fromPRMI']['ON']
        ezca['LSC-SRCL_TRIG_THRESH_OFF'] = lscparams.thresh['DRMI_SRCL_fromPRMI']['OFF']

class PRMI_TO_DRMI(GuardState):
    index = 49
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        ezca.get_LIGOFilter('SUS-BS_M3_ISCINF_L').switch_on('LIMIT')
        ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').switch('FM2', 'ON')
        # Change SRCL trigger
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = lscparams.thresh['DRMI_SRCL_fromPRMI']['ON']
        ezca['LSC-SRCL_TRIG_THRESH_OFF'] = lscparams.thresh['DRMI_SRCL_fromPRMI']['OFF']
        time.sleep(0.1)

        ezca.get_LIGOFilter('LSC-SRCL1').switch_on('INPUT','OUTPUT', wait=False)

        self.timer['wait'] = 4

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        if self.timer['wait']:
            if not ISC_library.is_locked('PRMI'):
                # Didn't make the transition to DRMI.  Turn off top stage feedback to PRM and clear history
                ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_L').ramp_gain(0, 2, wait=True)
                ezca.get_LIGOFilter('SUS-BS_M1_LOCK_L').switch_off('INPUT')
                ezca['SUS-PRM_M1_LOCK_L_RSET'] = 2

                return 'PREP_DRMI' # go through prep DRMI, acq DRMI, then to prep PRMI, acq PRMI, then here

            elif ISC_library.is_locked('DRMI'):
                log('Made it to DRMI from PRMI!') # Next, go to DRMI_LOCKED for offloading
                return True


##################################################
# STATES: DRMI ASC
##################################################

DRMI_WFS_CENTERING = ISC_GEN_STATES.gen_WFS_DC_CENTERING('DRMI','REFL_AS')
DRMI_WFS_CENTERING.index = 70


class PREP_DRMI_ASC(GuardState):
    index = 78
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        ezca['ASC-WFS_SWTCH'] = 1
        ezca['ASC-WFS_GAIN']  = 1

        for dof in ['MICH','INP1','INP2','PRC1','PRC2','SRC1','SRC2']:
            for py in ['PIT','YAW']:
                ISC_library.asc_intrix[py].put(dof,[],0) # Zero out rows of input matrix
                ISC_library.asc_outrix[py].put([],dof,0) # Zero out columns of output matrix

        # INPUT MATRIX
        for py in ['PIT','YAW']:
            # For settings that are the same pit and yaw
            ISC_library.asc_intrix[py]['MICH', 'AS_B_RF45_Q']  = -100
            ISC_library.asc_intrix[py]['PRC1', 'POP_A_DC']     = 1
            ISC_library.asc_intrix[py]['PRC2', 'REFL_A_RF9_I'] = 1*2  #double refl input matrix elements to account for HAM1 splitter June 23rd 2022
            ISC_library.asc_intrix[py]['PRC2', 'REFL_B_RF9_I'] = 1*2
            ISC_library.asc_intrix[py]['SRC2', 'AS_C_DC']      = 1
            ISC_library.asc_intrix[py]['SRC1', 'AS_A_RF72_Q'] = 1

        ISC_library.asc_intrix['PIT']['INP1', 'REFL_A_RF9_I'] = 1*2
        ISC_library.asc_intrix['PIT']['INP1', 'REFL_B_RF9_I'] = -1.6*2
        ISC_library.asc_intrix['YAW']['INP1', 'REFL_A_RF9_I'] = 1*2
        ISC_library.asc_intrix['YAW']['INP1', 'REFL_B_RF9_I'] = -1.3*2

        # OUTPUT MATRIX
        for py in ['PIT','YAW']:
            ISC_library.asc_outrix[py]['IM4', 'INP1'] = 1
            ISC_library.asc_outrix[py]['PRM', 'PRC1'] = 1
            ISC_library.asc_outrix[py]['PR2', 'PRC2'] = 1
            ISC_library.asc_outrix[py]['BS',  'MICH'] = 1
            ISC_library.asc_outrix[py]['SRM', 'SRC1'] = 1
            ISC_library.asc_outrix[py]['SR2', 'SRC2'] = 1
        ISC_library.asc_outrix['PIT']['SRM',  'SRC2'] = -7.6 # to remove SRM coupling into SR2
        ISC_library.asc_outrix['YAW']['SRM',  'SRC2'] = 7.1

        ezca['ASC-PRC1_P_LIMIT'] = 10000
        ezca['ASC-PRC1_Y_LIMIT'] = 10000
        ezca['ASC-SRC1_P_LIMIT'] = 1000
        ezca['ASC-SRC1_Y_LIMIT'] = 1000

        # SET UP CONTROL SETTINGS
        ezca.get_LIGOFilter('ASC-INP1_P').ramp_gain(0, ramp_time=0, wait=False)
        ezca.get_LIGOFilter('ASC-INP1_Y').ramp_gain(0, ramp_time=0, wait=False)

        ezca['ASC-INP1_P_RSET'] = 2
        ezca['ASC-INP1_Y_RSET'] = 2
        ezca['ASC-PRC1_P_RSET'] = 2
        ezca['ASC-PRC1_Y_RSET'] = 2


        time.sleep(0.5)

        ezca.get_LIGOFilter('ASC-INP1_P').only_on('INPUT','FM4','FM5','FM9','OUTPUT', 'DECIMATION')  # Using harder cutoff. 20Mar2019 JCD
        ezca.get_LIGOFilter('ASC-INP1_Y').only_on('INPUT','FM4','FM5','FM9','OUTPUT','DECIMATION')   # Using harder cutoff. 20Mar2019 JCD

        ezca.get_LIGOFilter('ASC-MICH_P').only_on('INPUT', 'FM1','FM3', 'FM9','FM10','OUTPUT', 'DECIMATION')# added in tuned BR for 17Hz mode J Wright 05/06/2024.
        #ezca.get_LIGOFilter('ASC-MICH_P').only_on('INPUT', 'FM1','FM4','FM8','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-MICH_Y').only_on('INPUT', 'FM1','FM3','FM4','FM7','FM9','FM10','OUTPUT', 'DECIMATION')#change LP from LP6 to ELP15 SED DDB March 17 2019, added in tuned BR for 17Hz mode J Wright 05/06/2024.

        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM2', 'FM3', 'FM4', 'FM7', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM2', 'FM3', 'FM4', 'FM7', 'OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = 2 #10 # lower by factor of 5 JCD 6Aug2019, alog 51077
        ezca['ASC-PRC1_Y_GAIN'] = 1

        ezca.get_LIGOFilter('ASC-PRC2_P').only_on('FM3', 'FM4', 'FM8','OUTPUT', 'DECIMATION') #EMC switch FM7 to FM8, modified plant inversion to prevent ringing
        ezca.get_LIGOFilter('ASC-PRC2_Y').only_on('FM3', 'FM4', 'FM8','OUTPUT','DECIMATION')#20190122 switched to match full ifo asc, was fm 2 3 4 5 10
        ezca['ASC-PRC2_P_GAIN'] = -0.5 #20220823 stop oscillations
        ezca['ASC-PRC2_Y_GAIN'] = -1 #20190122, increased for faster convergence, was -0.5

        #ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM3', 'FM5', 'FM10','OUTPUT','DECIMATION') # HY removed FM2
        #ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM3', 'FM5', 'FM10','OUTPUT','DECIMATION')
        #copied from prep_asc_for_full_ifo
        ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM1', 'FM3', 'FM4', 'FM5', 'FM10','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM1', 'FM3', 'FM4', 'FM5', 'FM10','OUTPUT', 'DECIMATION')
        ezca['ASC-SRC1_P_OFFSET'] = lscparams.asc_offset['SRC_AS72_P']  # Adding AS72 offsets also to DRMI ASC, vals from alog 78332. JCD 11June2024
        ezca['ASC-SRC1_Y_OFFSET'] = lscparams.asc_offset['SRC_AS72_Y']  # Adding AS72 offsets also to DRMI ASC, vals from alog 78332. JCD 11June2024

        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1', 'FM3', 'FM4', 'FM9','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1', 'FM3', 'FM4', 'FM9','OUTPUT', 'DECIMATION')
        ezca['ASC-SRC1_Y_RSET'] = 2
        time.sleep(1)
        #ezca['ASC-SRC1_P_GAIN'] = 9000 # Gain up to make mtrx elem match full IFO #commented out glm 20210430
        #ezca['ASC-SRC1_Y_GAIN'] = 7200 # Gain up to make mtrx elem match full IFO
        ezca['ASC-SRC1_P_SMOOTH_TRAMP'] = 1
        ezca['ASC-SRC1_P_SMOOTH_OFFSET_ENABLE'] = 1

        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM1', 'FM3', 'FM4', 'FM9','OUTPUT', 'DECIMATION') # HY removed FM2
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM1', 'FM3', 'FM4', 'FM9','FM10','OUTPUT', 'DECIMATION')
        #ezca['ASC-SRC2_P_GAIN'] = 60#commented out glm 20210430
        #ezca['ASC-SRC2_Y_GAIN'] = 60

        # SET UP SUSPENSIONS
        # set up IM4
        ezca.get_LIGOFilter('SUS-IM4_M1_LOCK_P').only_on('INPUT', 'FM1', 'FM2', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('SUS-IM4_M1_LOCK_Y').only_on('INPUT', 'FM1', 'FM2', 'OUTPUT', 'DECIMATION')

        # set up BS feedback to M2 only, hold M1
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P').only_on('FM1', 'FM2', 'FM5', 'FM9', 'LIMIT','OUTPUT', 'DECIMATION') #m1 input turned off
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y').only_on('FM1', 'FM5', 'FM9', 'LIMIT','OUTPUT', 'DECIMATION')
        # HY 09/04/18 adding extra LP0.5 (FM5) for the top stage
        ezca['SUS-BS_M2_LOCK_OUTSW_P'] = 1
        ezca['SUS-BS_M2_LOCK_OUTSW_Y'] = 1
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P').switch_on('INPUT') # Used to be in ENGAGE - why not here?
        ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y').switch_on('INPUT')# Used to be in ENGAGE - why not here?

        # set up PRM feedback to M3
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_P').only_on('INPUT','FM1','FM9', 'LIMIT','OUTPUT', 'DECIMATION') #m1 input is turned off
        ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_Y').only_on('INPUT','FM1','FM9', 'LIMIT','OUTPUT', 'DECIMATION')    #sed,crc turned on because it seems fine to use it see alog 43259 and comments
        ezca['SUS-PRM_M3_LOCK_OUTSW_P'] = 1
        ezca['SUS-PRM_M3_LOCK_OUTSW_Y'] = 1

        # set up PR2 feedback to M3
        ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_P').only_on('FM5','FM6', 'FM9', 'OUTPUT', 'LIMIT','DECIMATION')
        ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_Y').only_on('FM5','FM6', 'FM9', 'OUTPUT', 'LIMIT','DECIMATION')
        ezca['SUS-PR2_M3_LOCK_OUTSW_P'] = 1
        ezca['SUS-PR2_M3_LOCK_OUTSW_Y'] = 1
        ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_P').switch_on('INPUT')# Used to be in ENGAGE - why not here?
        ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_Y').switch_on('INPUT')# Used to be in ENGAGE - why not here?

        # set up SRM feedback to M3 only (and M1)
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P').only_on('OUTPUT','LIMIT', 'DECIMATION','FM3','FM5','FM8')#SED moved integrator from FM1 to FM5, Sept 2 2019
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y').only_on('OUTPUT','LIMIT','DECIMATION','FM3','FM5','FM8')
        ezca['SUS-SRM_M3_LOCK_OUTSW_P'] = 1
        ezca['SUS-SRM_M3_LOCK_OUTSW_Y'] = 1
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P').switch_on('INPUT')# Used to be in ENGAGE - why not here?
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y').switch_on('INPUT')# Used to be in ENGAGE - why not here?



        # set up SR2 feedback to M3 only
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P').only_on('OUTPUT','LIMIT', 'DECIMATION','FM3','FM5','FM8')#SED moved integrator from FM2 to FM5, Sept 2 2019
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y').only_on('OUTPUT', 'LIMIT','DECIMATION','FM3','FM5','FM8')
        ezca['SUS-SR2_M3_LOCK_OUTSW_P'] = 1
        ezca['SUS-SR2_M3_LOCK_OUTSW_Y'] = 1
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P').switch_on('INPUT')# Used to be in ENGAGE - why not here?
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y').switch_on('INPUT')# Used to be in ENGAGE - why not here?

        # Set up limiters for QPD dofs
        #for py in ['P','Y']:
        #    for dof in ['PRC1','SRC2']:
        #        ezca['ASC-%s_%s_SMOOTH_SMOOTH'%(dof,py)] = 1
        #        ezca['ASC-%s_%s_SMOOTH_LIMIT'%(dof,py)]  = 0.1
        #        ezca['ASC-%s_%s_SMOOTH_ENABLE'%(dof,py)] = 1


    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        if not ISC_library.WFS_DC_centering_servos_OK('REFL_AS'):
            notify('WFS DC centering railed')
        else:
            return True

class ENGAGE_DRMI_ASC(GuardState):
    index = 80
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        self.caution = False
        self.counter = 1
        self.timer['wait'] = 0
        self.timer['dt'] = 1./16.
        # RWS - all back to True post-vent 23Aug2024
        self.useMICH = True
        self.useINP1 = True
        self.usePRC1 = True  #returned to TRUE after adding new POP_A Pit OFFSET on feb7, 2025
        self.usePRC2 = True  #returned to TRUE after adding new POP_A Pit OFFSET on feb7, 2025
        self.useSRC1 = False
        self.useSRC2 = False


    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        if ezca['ASC-AS_C_NSUM_OUT16'] < lscparams.thresh['DRMI_ASC']['ON']:
            theMessage = 'AS_C_DC is too low! Align by hand! Not engaging ASC!'
            notify(theMessage)
            log(theMessage)
            return None
        elif ezca['ASC-AS_C_NSUM_OUT16'] < lscparams.thresh['DRMI_ASC']['CAUTION']:
            theMessage = 'AS_C_DC is kind of low! Engaging ASC with caution...'
            notify(theMessage)
            log(theMessage)
            self.caution = True

        if self.counter == 1:
            if self.useINP1:
                ezca.get_LIGOFilter('ASC-INP1_P').ramp_gain(-1, ramp_time=3, wait=False)
                ezca.get_LIGOFilter('ASC-INP1_Y').ramp_gain(-0.1, ramp_time=3, wait=False)

            if self.usePRC2:
                ezca.get_LIGOFilter('ASC-PRC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-PRC2_Y').switch_on('INPUT')

            if self.caution:
                log('10 sec sleep for slow ASC engage')
                self.timer['wait'] = 10

            if self.useMICH:
                #ezca.get_LIGOFilter('ASC-MICH_P').ramp_gain(-0.3, ramp_time=10, wait=False)
                ezca.get_LIGOFilter('ASC-MICH_P').ramp_gain(lscparams.asc_gains['MICH']['P']['ENGAGE_DRMI_ASC'], ramp_time=20, wait=False)
                # was -0.22 # SD, HY changed the MICH gain so that the UGF is 0.7 Hz; to avoid osc. at 1.1 Hz
                ezca.get_LIGOFilter('ASC-MICH_Y').ramp_gain(lscparams.asc_gains['MICH']['Y']['ENGAGE_DRMI_ASC'], ramp_time=20, wait=False)
                ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_P').ramp_gain(0, ramp_time=20, wait=False)
                ezca.get_LIGOFilter('SUS-BS_M2_OLDAMP_Y').ramp_gain(0, ramp_time=20, wait=False)

            self.counter += 1
            self.timer['wait'] = ezca['ASC-MICH_P_TRAMP']

            if self.caution:
                log('10 sec sleep for slow ASC engage')
                self.timer['wait'] = 90

        if self.counter == 2 and self.timer['wait']:
            if self.useMICH:
                log('skip gain incr for MICH')
                # Don't get rid of 20dB, causes osc unless alignment very good. JCD 18Jan2019
                #ezca.get_LIGOFilter('ASC-MICH_P').switch_off('FM1')
                #ezca.get_LIGOFilter('ASC-MICH_Y').switch_off('FM1')
            self.counter += 1
            self.timer['wait'] = 1

        if self.counter == 3 and self.timer['wait']:
            if self.useMICH:
                ezca.get_LIGOFilter('ASC-MICH_P').switch_on('FM2')
                ezca.get_LIGOFilter('ASC-MICH_Y').switch_on('FM2')

            self.counter += 1
            self.timer['wait'] = 4

        if self.counter == 4 and self.timer['wait']:
            if self.useSRC1:
                ezca.get_LIGOFilter('ASC-SRC1_P').switch_on('INPUT','OFFSET')
                ezca.get_LIGOFilter('ASC-SRC1_P').ramp_gain(4, ramp_time=5, wait=False)
                ezca.get_LIGOFilter('ASC-SRC1_Y').switch_on('INPUT','OFFSET')
                ezca.get_LIGOFilter('ASC-SRC1_Y').ramp_gain(4, ramp_time=5, wait=False)
            if self.useSRC2:
                ezca.get_LIGOFilter('ASC-SRC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-SRC2_P').ramp_gain(40, ramp_time=5, wait=False)
                ezca.get_LIGOFilter('ASC-SRC2_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-SRC2_Y').ramp_gain(100, ramp_time=5, wait=False)
            if self.caution:
                log('10 sec sleep for slow ASC engage')
                self.timer['wait'] = 10

            self.counter += 1
            self.timer['wait'] = 2

        if self.counter == 5 and self.timer['wait']:
            if self.usePRC1:
                ezca.get_LIGOFilter('ASC-PRC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-PRC1_Y').switch_on('INPUT')
            self.counter += 1

        if self.counter == 6 and self.timer['wait']:
            return True


class DRMI_1F_LOCKED_W_ASC(GuardState):
    index = 100
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        ezca['LSC-SRCL1_TRAMP'] =  5
        time.sleep(0.1)
        ezca.get_LIGOFilter('LSC-SRCL1').switch_off('OFFSET')

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        return True


OFFLOAD_DRMI_ASC = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('DRMI', 5, ['BS','PR2','PRM','SR2','SRM','IM4'])
OFFLOAD_DRMI_ASC.index = 101


class DRMI_WFS_OFFLOADED(GuardState):
    index = 102
    request = True
    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    #@nodes.checker()
    def main(self):
        # Get rid of limiters for QPD dofs, just in case
        for py in ['P','Y']:
            for dof in ['PRC1','SRC2']:
                ezca['ASC-%s_%s_SMOOTH_ENABLE'%(dof,py)] = 0
        return True


##################################################
# STATES: DRMI 3F
##################################################

class ZERO_3F_OFFSETS(GuardState):
    index = 110
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        ezca.get_LIGOFilter('LSC-SRCL1').switch_off('OFFSET')
        offsets = cdu.avg(5, ['LSC-REFLAIR_B_RF27_I_INMON',
                                   'LSC-REFLAIR_B_RF27_Q_INMON',
                                   'LSC-REFLAIR_B_RF135_I_INMON',
                                   'LSC-REFLAIR_B_RF135_Q_INMON'],
                               )
        # write offsets
        ezca['LSC-REFLAIR_B_RF27_I_OFFSET']  = -round(offsets[0], 3)
        ezca['LSC-REFLAIR_B_RF27_Q_OFFSET']  = -round(offsets[1], 3)
        ezca['LSC-REFLAIR_B_RF135_I_OFFSET'] = -round(offsets[2], 3)
        ezca['LSC-REFLAIR_B_RF135_Q_OFFSET'] = -round(offsets[3], 3)

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        return True

class TRANSITION_TO_3F(GuardState):
    index = 120
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 1

        ISC_library.intrix.put('MICH',[],0) # Zero out MICH row of input matrix, removing 1F elements
        ISC_library.intrix.put('PRCL',[],0) # Zero out PRCL row of input matrix, removing 1F elements
        ISC_library.intrix.put('SRCL',[],0) # Zero out SRCL row of input matrix, removing 1F elements

        ISC_library.intrix['PRCL', 'REFLAIR_B27I']  = lscparams.gain['DRMI_PRCL']['3F_27']
        ISC_library.intrix['MICH', 'REFLAIR_B135Q'] = lscparams.gain['DRMI_MICH']['3F_135']
        ISC_library.intrix['MICH', 'REFLAIR_B27I'] = lscparams.gain['DRMI_MICH']['3F_27I']
        ISC_library.intrix['SRCL', 'REFLAIR_B135I'] = lscparams.gain['DRMI_SRCL']['3F_135']
        ISC_library.intrix['SRCL', 'REFLAIR_B27I']  = lscparams.gain['DRMI_SRCL']['3F_27']

        ISC_library.intrix.load()
        self.timer['pause'] = ezca['LSC-PD_DOF_MTRX_TRAMP'] + 1

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        if self.timer['pause']:
            return True


class DRMI_3F_LOCKED(GuardState):
    index = 130
    request = True

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        # Switch M3 stages to low noise.
        for sus in ['PRM','SRM']:
            for section in ['UR','UL','LR','LL']:
                ezca['SUS-{}_BIO_M3_{}_STATEREQ'.format(sus, section)] = 1
            #turn up gain for PRM, SRM offloading (in M2 becaue the integrator is in FM1)
            ezca.get_LIGOFilter('SUS-%s_M2_LOCK_L'%sus).ramp_gain(10, ramp_time=5, wait=False)

        self.timer['t_DRMI_3f'] = 10

    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def run(self):
        if self.timer['t_DRMI_3f']:
            return True


OFFLOAD_DRMI_ASC_3F = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('DRMI', 5, ['BS','PR2','PRM','SR2','SRM','IM4'])
OFFLOAD_DRMI_ASC_3F.index = 131


class DRMI_3F_W_ASC_READY(GuardState):
    index = 132
    request = True
    @ISC_library.assert_dof_locked_gen(['IMC','DRMI'])
    def main(self):
        return True

##################################################
# States: SRMI
##################################################

class PREP_SRMI(GuardState):
    index = 300
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):

        ISC_library.intrix.put('SRCL',[],0) # Zero out SRCL row of input matrix
        ISC_library.intrix.put('MICH',[],0) # Zero out SRCL row of input matrix
        #ISC_library.intrix.put('MICH','POP_A45Q', -40)
        ISC_library.intrix.put('MICH','REFL_A45Q', -100000)
        ISC_library.intrix.put('SRCL','ASAIR_A45I',10000)
        ezca['LSC-PD_DOF_MTRX_LOAD_MATRIX'] = 1

        ISC_library.trigrix.put('SRCL',[],0) # Zero out SRCL row of input matrix
        ISC_library.trigrix.put('MICH',[],0) # Zero out SRCL row of input matrix
        ISC_library.trigrix['MICH', 'AS_C_NSUM'] = 10
        ISC_library.trigrix['SRCL', 'AS_C_NSUM'] = 10

        ezca['LSC-SRCL_TRIG_THRESH_ON'] = -100
        ezca['LSC-SRCL_TRIG_THRESH_OFF'] = -100
        ezca['LSC-MICH_TRIG_THRESH_ON'] = -100#lscparams.thresh['SRMI_MICH']['ON']
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = -100#lscparams.thresh['SRMI_MICH']['OFF']

        ezca.get_LIGOFilter('LSC-MICH1').only_on('INPUT','OUTPUT')
        ezca.get_LIGOFilter('LSC-MICH2').only_on('INPUT','FM1','FM2','OUTPUT', wait=False)
        ezca.get_LIGOFilter('SUS-BS_M2_LOCK_L').only_on('INPUT', 'FM4', 'FM5', 'FM9', 'FM10', 'OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('SUS-BS_M3_LOCK_L').only_on('INPUT', 'FM1', 'FM2', 'OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('SUS-BS_M3_ISCINF_L').only_on('INPUT', 'FM6', 'FM10', 'OUTPUT','DECIMATION')

        ezca.get_LIGOFilter('LSC-SRCL1').only_on('INPUT','FM8','FM9','FM10','OUTPUT','DECIMATION', wait=False)
        ezca.get_LIGOFilter('LSC-SRCL2').only_on('INPUT','OUTPUT','DECIMATION', wait=False)
        ezca.get_LIGOFilter('SUS-SRM_M3_ISCINF_L').only_on('INPUT','OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('SUS-SRM_M3_LOCK_L').only_on('INPUT','OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').only_on('INPUT','OUTPUT','DECIMATION')
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_L').only_on('FM1', 'FM2','FM4','FM5','FM6','FM8','FM10','OUTPUT','DECIMATION')
        time.sleep(0.1)
        ezca['LSC-MICH1_TRAMP'] = 2

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        return True

class LOCK_SRMI(GuardState):
    index = 301
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        self.ramptime=3
        ezca.get_LIGOFilter('LSC-MICH1').ramp_gain(4000, ramp_time=self.ramptime, wait=False)
        self.timer['wait'] = self.ramptime
        self.counter = 1

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        if self.timer['wait'] and (self.counter==1):
           ezca.get_LIGOFilter('LSC-MICH1').switch_on('FM2', wait=False)
           self.counter = 2
        return True
##################################################
# EDGES
##################################################

edges = [
    ('IDLE',                 'DOWN'),
    ('LOCKLOSS',             'DOWN'),
    ('DOWN',                 'DOWN'),#adding an edge from DOWN to DOWN, this way when we are in DOWN and ISC_LCOK requests down, the main will be run again.
    # DRMI length
    ('DOWN',                 'PREP_DRMI'),
    ('PREP_DRMI',            'ACQUIRE_DRMI_1F'),
    ('ACQUIRE_DRMI_1F',      'DRMI_LOCKED'),
    ('DRMI_LOCKED',          'DRMI_1F_READY'),
    # PRMI length
    ('ACQUIRE_DRMI_1F',      'PREP_PRMI'),
    ('PREP_PRMI',            'ACQUIRE_PRMI'),
    ('ACQUIRE_PRMI',         'PRMI_LOCKED'),
    ('ACQUIRE_PRMI',         'PREP_DRMI'),
    ('PRMI_LOCKED',          'PRMI_WFS_CENTERING'),
    ('PRMI_WFS_CENTERING',   'PREP_PRMI_ASC'),
    ('PREP_PRMI_ASC',        'ENGAGE_PRMI_ASC'),
    ('ENGAGE_PRMI_ASC',      'PRMI_ASC'),
    ('PRMI_ASC',             'OFFLOAD_PRMI_ASC'),
    ('OFFLOAD_PRMI_ASC',     'PRMI_ASC_OFFLOADED'),
    ('PRMI_ASC_OFFLOADED',   'PREP_PRMI_TO_DRMI'),
    ('PRMI_LOCKED',          'PREP_PRMI_TO_DRMI'),
    ('PREP_PRMI_TO_DRMI',    'PRMI_TO_DRMI'),
    ('PRMI_TO_DRMI',         'DRMI_LOCKED'),
    ('PREP_PRMI',         'ACQUIRE_PRMI_CARRIER'),
    ('ACQUIRE_PRMI_CARRIER', 'PREP_PRMI'),
    # PRMI ASC
    #('PRMI_WFS_CENTERING',   'PREP_PRMI_ASC'),
    #('PREP_PRMI_ASC',        'ENGAGE_PRMI_ASC'),
    # DRMI ASC
    ('DRMI_1F_READY',        'DRMI_WFS_CENTERING'),
    ('DRMI_WFS_CENTERING',   'PREP_DRMI_ASC'),
    ('PREP_DRMI_ASC',        'ENGAGE_DRMI_ASC'),
    ('ENGAGE_DRMI_ASC',      'DRMI_1F_LOCKED_W_ASC'),
    ('DRMI_1F_LOCKED_W_ASC', 'OFFLOAD_DRMI_ASC'),
    ('OFFLOAD_DRMI_ASC',     'DRMI_WFS_OFFLOADED'),
    # DRMI 3F
    #('PREP_DRMI_ASC',        'ZERO_3F_OFFSETS'),  #this edge allows one to skip DRMI ASC using manual on the ISC_LOCK guardian
    ('DRMI_1F_LOCKED_W_ASC', 'ZERO_3F_OFFSETS'),
    ('ZERO_3F_OFFSETS',      'TRANSITION_TO_3F'),
    ('TRANSITION_TO_3F',     'DRMI_3F_LOCKED'),
    ('DRMI_3F_LOCKED',       'OFFLOAD_DRMI_ASC_3F'),
    ('OFFLOAD_DRMI_ASC_3F',  'DRMI_3F_W_ASC_READY'),
    ('DRMI_3F_W_ASC_READY',  'IDLE'),
    # SRMI
    ('DOWN','PREP_SRMI'),
    ('PREP_SRMI', 'LOCK_SRMI'),
]
